import unittest
from app import app

class TestLocalRequestExactMatch(unittest.TestCase):

    def setUp(self):
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()

    def tearDown(self):
        self.ctx.pop()

    def test_exact_match_request(self):
        response = self.client.get(
            "/METAKB/v1/GenomicClinicalEvidence?variants=BRAF:V600E&genompos=chr7:140753336A>T&size=1000&key=genompos")
        print(response.get_data(as_text=True))
