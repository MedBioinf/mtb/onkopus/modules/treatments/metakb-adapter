import unittest
from metakb_adapter.api_request import MetaKB
from metakb_adapter.normalize import map_evidence_levels

class TestNormalization(unittest.TestCase):

    def test_evidence_level_mapping(self):
        variant_data = {
            "chr1:114713908T>A": {
                "metakb_features_norm": {
                    "exact_match": [
                        {
                            "citation_id": "21576590",
                            "disease": "Melanoma",
                            "evidence_level": 3
                        }
                    ]
                }
            }
        }
        gene ="NRAS"

        obj = MetaKB("NRAS", input_variant="Q61L",key_arg="genompos")
        obj.exact_match_request()
        obj.normalize()
        data = obj.get_full_response()

        variant_data = map_evidence_levels(data)
        print(variant_data)
