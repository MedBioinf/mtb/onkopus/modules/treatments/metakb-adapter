import unittest
import metakb_adapter.local_request


class TestLocalRequestAnyMutationInGene(unittest.TestCase):

    def test_any_mutation_in_gene_request(self):
        request_obj = metakb_adapter.local_request.LocalMetaKB()
        response = metakb_adapter.local_request.any_mutation_in_gene_request(request_obj.gene_dc,"BRAF")
        print(response)
