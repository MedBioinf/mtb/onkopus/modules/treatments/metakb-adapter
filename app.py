import logging
import os, copy
from flask import Flask, request, send_from_directory
from flask_swagger_ui import get_swaggerui_blueprint
from metakb_adapter.api_request import MetaKB
from metakb_adapter.helper import get_metadata
from metakb_adapter.normalize import map_evidence_levels, normalize_feature_values
import metakb_adapter.local_request
from metakb_adapter.tools.parse import merge_dictionaries


DEBUGGING = True
SERVICE_NAME = "METAKB"
VERSION = 'v1'

if DEBUGGING:
    logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)
app.config.from_object(__name__)

app = Flask(__name__)

# Set up SwaggerUI
SWAGGER_URL = f'/{SERVICE_NAME}/{VERSION}/doc/'
API_URL = f'/{SERVICE_NAME}/{VERSION}/openapi/'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': f"{SERVICE_NAME}"
    }
)

local_metakb = metakb_adapter.local_request.LocalMetaKB()


@app.route(f'/{SERVICE_NAME}/{VERSION}/GenomicClinicalEvidence')
def genomic_clinical_evidence():
    input_variants = request.args.get('variants')
    input_variants = input_variants.split(',')
    query_ids = request.args.get('genompos').split(",")
    key_arg = request.args.get('key')

    response = {}
    for i,variant in enumerate(input_variants):
        if key_arg is not None:
            qid = query_ids[i]
        else:
            qid = variant

        response[qid] = {}

        if variant != '':
            # Split input variant of type '<Gene>:<Variant>'
            variant_split = variant.split(':')

            if len(variant_split)>1:
                temp_gene = variant_split[0]
                temp_variant = variant_split[1]
            else:
                temp_gene = variant
                temp_variant = None

            response_full = {}

            # Exact match request
            response_variant = metakb_adapter.local_request.exact_match_request_biomarker(local_metakb.biomarker_dc,temp_gene + " " + temp_variant)
            response_full = merge_dictionaries(response_full, copy.copy(response_variant))

            # Other mutation at same position request
            #ref,pos,alt=generate
            #substitutes = generate_possible_substitutes(temp_variant)
            #for substitute in substitutes:
            #    q = temp_gene + " " + ref + pos + substitute
            #    response_variant = metakb_adapter.local_request.metakb_request(local_metakb.biomarker_dc, q,
            #                                                                   "any_mutation_in_gene")
            #    response_full = merge_dictionaries(response_full, copy.copy(response_variant))

            # Any mutation in gene request
            q = temp_gene + " mutant"
            response_variant = metakb_adapter.local_request.metakb_request(local_metakb.biomarker_dc, q, "any_mutation_in_gene")
            response_full = merge_dictionaries(response_full, copy.copy(response_variant))
            q = temp_gene + " mut"
            response_variant = metakb_adapter.local_request.metakb_request(local_metakb.biomarker_dc, q, "any_mutation_in_gene")
            response_full = merge_dictionaries(response_full, copy.copy(response_variant))

            response[qid] = response_full

    response = map_evidence_levels(response)
    response = normalize_feature_values(response)
    response = metakb_adapter.tools.filter_empty_citation_ids(response)
    return response


@app.route(f'/{SERVICE_NAME}/{VERSION}/full')
def full_request():
    """

    :return:
    """
    input_variants = request.args.get('variants')
    input_variants = input_variants.split(',')
    input_size = request.args.get('size')
    query_ids = request.args.get('genompos')
    key_arg = request.args.get('key')

    if query_ids is not None:
        query_id_split = query_ids.split(',')
    else:
        query_id_split = []

    filling_dict = {}
    response={}
    request_objs = {}
    for i,variant in enumerate(input_variants):
        request_obj=None
        if variant != '':
            # Split input variant of type '<Gene>:<Variant>'
            variant_split = variant.split(':')

            if len(variant_split)>1:
                temp_gene = variant_split[0]
                temp_variant = variant_split[1]
            else:
                temp_gene = variant
                temp_variant = None

            if temp_gene != '':
                if len(query_id_split) > i:
                    genompos = query_id_split[i]
                else:
                    genompos=None

                request_obj = MetaKB(temp_gene, input_variant=temp_variant, response_size=input_size, genompos=genompos, key_arg=key_arg)
                request_obj.exact_match_request()

                request_obj.gene_request()
                request_obj.normalize()
                request_objs.update(request_obj.get_full_response() )

    return request_objs


@app.route(f'/{SERVICE_NAME}/{VERSION}/openapi/')
def get_openapi():
    """

    :return:
    """
    return send_from_directory(os.getcwd(), "res/conf/openapi.json")


@app.route(f'/{SERVICE_NAME}/{VERSION}/info')
def info():
    return get_metadata()


if __name__ == '__main__':
    app.register_blueprint(SWAGGERUI_BLUEPRINT)
    app.run(host='0.0.0.0', debug=True, port=8100)
