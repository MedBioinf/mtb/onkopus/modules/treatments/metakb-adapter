import copy
import metakb_adapter.conf.read_config as conf_reader
from metakb_adapter.tools.parse import extract_pmid

# MetaKB keys
metakb_extract_keys = ['diseases', 'drugs', 'evidence_label', 'response', 'source', 'publications']
# association
metakb_extract_keys_1 = ['description', 'variant_name', 'evidence_level', 'response_type']
# gene_identifiers
metakb_extract_keys_2 = ['ensemble_gene_id', 'entrez_id', 'symbol']
# features
metakb_extract_keys_3 = ['geneSymbol', 'biomarker_type', 'chromosome', 'alt', 'ref']
metakb_extract_keys_agg = ['geneSymbol', 'variant_name', 'diseases', 'drugs', 'evidence_level', '', 'citation_url',
                                'citation_id', 'source', 'response_type', 'description']
agg_keys = ['gene', 'variant', 'disease', 'drugs', 'evidence_level', 'evidence_type', 'citation_url',
                 'citation_id',
                 'source', 'response', 'evidence_statement']


def map_evidence_levels(variant_data):
    """
    Maps MetaKB evidence level to the Onkopus evidence level system

    :param variant_data:
    :return:
    """
    for var in variant_data.keys():
        if 'metakb_features_norm' in variant_data[var]:
            for match_type in conf_reader.match_types:
                if match_type in variant_data[var]['metakb_features_norm']:
                    for result in variant_data[var]['metakb_features_norm'][match_type]:
                        if result["evidence_level"] in conf_reader.metakb_levels:
                            index = conf_reader.metakb_levels.index(result["evidence_level"])
                            result["evidence_level_onkopus"] = conf_reader.onkopus_levels[index]

    return variant_data


def normalize_feature_values(variant_data):
    """
    Normalizes clinical significance data

    :param variant_data:
    :return:
    """

    for var in variant_data.keys():
        if "metakb_features_norm" in variant_data[var]:
            for match_type in conf_reader.match_types:
                if match_type in variant_data[var]["metakb_features_norm"]:
                    for t,_ in enumerate(variant_data[var]["metakb_features_norm"][match_type]):
                        if "disease" in variant_data[var]["metakb_features_norm"][match_type][t]:
                            val_full = variant_data[var]["metakb_features_norm"][match_type][t]["disease"]
                            val_norm = ""
                            for val in val_full.split(" "):
                                val = val.lower()
                                val = val.replace("(disease)" ,"")
                                val = val.capitalize()
                                val_norm += val + " "
                            val_norm = val_norm.rstrip(" ")
                            variant_data[var]["metakb_features_norm"][match_type][t]["disease"] = val_norm

                        if "drugs" in variant_data[var]["metakb_features_norm"][match_type][t]:
                            for i, drug in enumerate(variant_data[var]["metakb_features_norm"][match_type][t]["drugs"]):
                                val = drug["drug_name"]
                                val = val.lower()
                                val = val.capitalize()
                                variant_data[var]["metakb_features_norm"][match_type][t]["drugs"][i]["drug_name"] = val

                        if "response" in variant_data[var]["metakb_features_norm"][match_type][t]:
                            val = variant_data[var]["metakb_features_norm"][match_type][t]["response"]
                            if val is not None:
                                val_new_raw = copy.deepcopy(val.lower())
                                val_new_raw = process_synonyms(val_new_raw)
                                val_new = ""
                                if val_new_raw == "sensitivity":
                                    val_new = "Sensitive"
                                elif val_new_raw == "resistant":
                                    val_new = "Resistant"
                                elif val_new_raw == "sensitive":
                                    val_new = "Sensitive"
                                elif val_new_raw == "Sensitive":
                                    val_new = val_new_raw
                                else:
                                    print("Could not assign response type: ",val_new_raw)
                                #    val_new = val_new_raw
                                val_new = val_new.capitalize()
                                #print(variant_data[var]["metakb_features_norm"][match_type][t]["drugs"], ":",
                                #      val_new_raw, " norm ", val_new)
                                variant_data[var]["metakb_features_norm"][match_type][t]["response"] = val_new

                        variant_data[var]["metakb_features_norm"][match_type][t]["match_type"] = match_type

    return variant_data


def process_synonyms(val):
    """
    Normalizes response type according to known synonyms

    :param val:
    :return:
    """
    dc = {
        "predicted - sensitive": "Sensitive",
        "resistance or non-response": "resistant",
        "sensitive": "Sensitive"
    }
    if val in dc.keys():
        return dc[val]
    else:
        return val


def normalize(json_obj):

    json_obj = normalize_feature_keys(json_obj)
    json_obj = normalize_feature_values(json_obj)
    #variant_data_new = self.filter_missing_values(variant_data_new)
    json_obj = map_evidence_levels(json_obj)

    return json_obj

def normalize_features(variant_data):
    """
    Selects a set of features that are present in all evidence databases from the MetaKB API features and assigns the same names to all feature keys

    :param variant_data: a JSON dictionary with genomic locations of variants as keys
    :return:
    """
    variant_data_new = copy.deepcopy(variant_data)

    variant_data_new = normalize_feature_keys(variant_data_new)
    variant_data_new = normalize_feature_values(variant_data_new)
    #variant_data_new = self.filter_missing_values(variant_data_new)
    variant_data_new = map_evidence_levels(variant_data_new)

    return variant_data_new


def normalize_feature_keys(variant_data):
    """
    Map features from the MetaKB API to the normalized data section

    :param variant_data:
    :return:
    """

    for var in variant_data.keys():
        if conf_reader.config['MODULES']['METAKB_PREFIX'] in variant_data[var]:

            for match_type in conf_reader.all_match_types:
                metakb_data = variant_data[var][conf_reader.config['MODULES']['METAKB_PREFIX']][match_type]

                if "metakb_features_norm" not in variant_data[var]:
                    variant_data[var]["metakb_features_norm"] = {}

                variant_data[var]['metakb_features_norm'][match_type] = []

                for i,result in enumerate(metakb_data):

                    urls = metakb_data[i]['publications'].split(",")
                    for url in urls:

                        metakb_extracted_data = {}

                        genes = [] #metakb_extracted_data[i]["genes"]
                        features = []
                        #metakb_extracted_data['genes'] = genes
                        #for gene in genes:

                        if 'features' in metakb_data[i]:
                            for feature in metakb_data[i]['features']:
                                gene = feature['geneSymbol']
                                variant_exchange = feature['name']
                                features.append(variant_exchange)
                                genes.append(gene)

                            metakb_extracted_data['genes'] = genes
                            metakb_extracted_data['variant_exchange'] = features

                            # metakb_extracted_data['biomarker'] = metakb_data[i]['feature_names']
                            metakb_extracted_data['biomarker'] = ''
                            for c, gene in enumerate(genes):
                                if (genes[c] is not None) and (features[c] is not None):
                                    metakb_extracted_data['biomarker'] += genes[c] + " " + features[c] + ", "
                            metakb_extracted_data['biomarker'] = metakb_extracted_data['biomarker'].rstrip(", ")
                        else:
                            metakb_extracted_data['biomarker'] = metakb_data[i]['feature_names']

                        #metakb_extracted_data['citation_id'] = metakb_data[i]['publications']
                        citation_id = extract_pmid(url)
                        metakb_extracted_data['citation_id'] = citation_id
                        metakb_extracted_data['citation_url'] = url

                        if 'response' in metakb_data[i]:
                            metakb_extracted_data['response_type'] = metakb_data[i]['response']
                        else:
                            metakb_extracted_data['response_type'] = ''

                        if 'drugs' in metakb_data[i]:
                            metakb_extracted_data['drugs'] = []
                            if "environmentalContexts" in metakb_data[i]['association']:
                                for drug in metakb_data[i]['association']['environmentalContexts']:
                                    dc = {}
                                    dc['drug_name'] = drug['description']
                                    #dc['id'] = drug['id']
                                    metakb_extracted_data['drugs'].append(dc)
                        else:
                            metakb_extracted_data['drugs'] = []

                        metakb_extracted_data['evidence_level'] = metakb_data[i]['evidence_label']
                        metakb_extracted_data['evidence_type'] = metakb_data[i]['association']['evidence'][0]['description']
                        metakb_extracted_data['evidence_statement'] = metakb_data[i]['association']['description']

                        if "diseases" in metakb_data[i]:
                            metakb_extracted_data['disease'] = metakb_data[i]['diseases']

                        metakb_extracted_data["source"] = "metakb_" + metakb_data[i]['source']

                        variant_data[var]['metakb_features_norm'][match_type].append(copy.deepcopy(metakb_extracted_data))

    #variant_data = self.split_multiple_pmids_in_separate_evidence_results(variant_data)

    return variant_data

def extract_metakb_data(variant_data):
    """
    Selects a set of features from the MetaKB API response and store them in an additional section in key-value format

    :param variant_data:
    :return:
    """
    for var in variant_data.keys():
        if conf_reader.config['MODULES']['METAKB_PREFIX'] in variant_data[var]:
            metakb_data = variant_data[var][conf_reader.config['MODULES']['METAKB_PREFIX']]

            for i,result in enumerate(metakb_data):

                metakb_extracted_data = {}
                for key in metakb_extract_keys:
                    if key in metakb_data[i]:
                        metakb_extracted_data[key] = metakb_data[i][key]
                        if key == 'publications':
                            pm_ids = metakb_data[i][key].split(",")
                            if pm_ids is not None:
                                metakb_extracted_data['citation_id'] = [x for x in pm_ids]
                                metakb_extracted_data['citation_url'] = [x for x in pm_ids]
                        elif key == 'source':
                            metakb_extracted_data[key] = 'metakb_' + metakb_data[i][key]
                for key in metakb_extract_keys_1:
                    if key in metakb_data[i]['association']:
                        if key == 'variant_name':
                            if type(metakb_data[i]['association'][key]) == str:
                                metakb_extracted_data[key] = metakb_data[i]['association'][key]
                            else:
                                metakb_extracted_data[key] = metakb_data[i]['association'][key][0]
                        else:
                            metakb_extracted_data[key] = metakb_data[i]['association'][key]
                for key in metakb_extract_keys_2:
                    if key in metakb_data[i]['gene_identifiers'][0]:
                        metakb_extracted_data[key] = metakb_data[i]['gene_identifiers'][0][key]
                for key in metakb_extract_keys_3:
                    if key in metakb_data[i]['features'][0]:
                        metakb_extracted_data[key] = metakb_data[i]['features'][0][key]

                if 'metakb_features' not in variant_data[var]:
                    variant_data[var]['metakb_features']=[]

                metakb_extracted_data["biomarker"] = metakb_extracted_data["geneSymbol"] + " " + metakb_extracted_data["variant_name"]

                # append clinical significance data
                variant_data[var]['metakb_features'].append(copy.deepcopy(metakb_extracted_data))

    variant_data = split_multiple_pmids_in_separate_evidence_results(variant_data)
    return variant_data

def split_multiple_pmids_in_separate_evidence_results(variant_data):

    for var in variant_data.keys():
        if "metakb_features" in variant_data[var]:
            treatments_new = []
            for result in variant_data[var]["metakb_features"]:
                pmid_urls = result['citation_id']

                for pmid_url in pmid_urls:
                    result_new = { }
                    for feature in result:
                        result_new[feature] = copy.deepcopy(result[feature])
                        result_new["citation_url"] = pmid_url
                        result_new["citation_id"] = extract_pmid(pmid_url)

                    treatments_new.append(result_new)
            variant_data[var]["metakb_features"] = treatments_new
    return variant_data
