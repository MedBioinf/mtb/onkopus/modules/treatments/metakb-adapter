import requests


def get_metadata():

    headers = {'PRIVATE-TOKEN': 'YqQhg-yv_3vG7t9ubDf3'}
    metadata = requests.get(
        'https://gitlab.gwdg.de/api/v4/projects/25875/repository/branches/main',
        headers=headers).json()
    meta_out = {"title": 'MetaKB Adapter'}
    meta_out["newest_commit_id"] = metadata["commit"]["short_id"]
    meta_out["newest_commit_message"] = metadata["commit"]["title"]
    meta_out["newest_commit_date"] = metadata["commit"]["authored_date"].split("T")[0]
    meta_out["data"] = {'uta': {'version': '20210129', 'date': '2021-01-29'}}
    meta_out["current_commit_id"] = metadata["commit"]["short_id"]
    meta_out["current_commit_date"] = metadata["commit"]["authored_date"]

    return meta_out
