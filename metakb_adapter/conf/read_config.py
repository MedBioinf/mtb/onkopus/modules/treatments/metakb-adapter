import os, configparser

config = configparser.ConfigParser()
config.read(os.path.join(os.path.dirname(__file__), '', 'config.ini'))

if "MODULE_SERVER" in os.environ:
    __MODULE_SERVER__ = os.getenv("MODULE_SERVER")
else:
    __MODULE_SERVER__ = config['DEFAULT']['MODULE_SERVER']

if "METAKB_DATA_DIR" in os.environ:
    __METAKB_DATA_DIR__ = os.getenv("METAKB_DATA_DIR")
else:
    __METAKB_DATA_DIR__ = config['DEFAULT']['METAKB_DATA_DIR']

pmid_expressions = config["SOURCE_SPECIFIC"]["PMID_URL_EXP"].split(" ")

# evidence level mapping
metakb_levels = ["A", "B", "C", "D"]
onkopus_levels = ["1", "2", "3", "4"]

match_types = ["exact_match", "any_mutation_in_gene", "same_position", "same_position_any_mutation"]
all_match_types = ["exact_match", "any_mutation_in_gene", "same_position", "same_position_any_mutation","nearby_mutations"]

