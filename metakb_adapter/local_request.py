import copy
import copyreg
import json
import time
import metakb_adapter.conf.read_config as conf_reader
from metakb_adapter.tools.parse import extract_pmid
from metakb_adapter.normalize import map_evidence_levels


class LocalMetaKB:

    def __init__(self):
        self.db_labels = ["JAX_CKB", "MolecularMatch", "PMKB", "CGI", "BRCA", "OncoKB", "CIViC"]
        self.file_names = ["jax.vr.json", "molecularmatch.vr.json", "pmkb.vr.json", "cgi.vr.json", "brca.vr.json", "oncokb.vr.json", "civic.vr.json"]
        self.gene_dc, self.biomarker_dc = self.load_dictionaries()
        self.response = {
            "metakb_features_norm":
                {
                    "exact_match":{},
                    "any_mutation_in_gene":{}
                }
        }


        print("Loaded gene dictionaries: ",self.gene_dc["JAX_CKB"].keys())
        print("Loaded biomarker dictionaries: ", self.biomarker_dc["JAX_CKB"].keys())

    def get_response(self):
        return self.response

    def load_dictionaries(self):
        """
        Loads database files into dictionaries sorted by 1) gene and 2) gene name and protein change

        :return:
        """
        gene_dc = {}
        biomarker_dc = {}

        for label, file_name in zip(self.db_labels, self.file_names):
            dc = self.load_lines(label, file_name)
            gene_dc[label] = self.load_gene_based_dictionary(label, dc)
            biomarker_dc[label] = self.load_biomarker_based_dictionary(dc)

        return gene_dc, biomarker_dc

    def load_lines(self,db_label, file_name):
        """
        Loads the database files and returns a list of dictionaries with normalized data

        :param db_label:
        :param file_name:
        :return:
        """
        dict_list = []
        with open(conf_reader.__METAKB_DATA_DIR__ + '/' + file_name,'r') as file:
            for i,gene in enumerate(file.readlines()):
                #print(gene)
                molecular_object = json.loads(gene)

                if len(molecular_object['features']) > 0:
                    new_dict = {}
                    new_dict['id'] = i
                    new_dict['source'] = "metakb_" + molecular_object['source']
                    new_dict['drugs'] = []
                    if "drug_labels" in molecular_object['association']:
                        drugs = molecular_object['association']['drug_labels'].split(",")
                        for drug in drugs:
                            drug_dict = {}
                            drug_dict['drug_name'] = drug
                            new_dict["drugs"].append(drug_dict)

                    #if "environmentalContexts" in molecular_object['association']:
                    #    molecular_object['drugs'] = []
                    #    for drug in molecular_object['association']['environmentalContexts']:
                    #        dc = {}
                    #        dc['drug_name'] = drug['description']
                    #        molecular_object['drugs'].append(dc)

                    if 'evidence' in molecular_object['association']:
                        if len (molecular_object['association']['evidence']) > 0:
                            if 'info' in molecular_object['association']['evidence'][0]:
                                if molecular_object['association']['evidence'][0]['info'] is not None:
                                    if 'publications' in molecular_object['association']['evidence'][0]['info']:
                                        new_dict['publication'] = molecular_object['association']['evidence'][0]['info']['publications']
                                else:
                                    continue
                    new_dict['evidence_level'] = molecular_object['association']['evidence_label']
                    new_dict['gene'] = molecular_object['features'][0]['geneSymbol']
                    new_dict['variant_exchange'] = molecular_object['features'][0]['name']

                    if (new_dict["gene"] is not None) and (new_dict["variant_exchange"]):
                        biomarker = new_dict["gene"] + " " + new_dict["variant_exchange"]
                    else:
                        continue
                    new_dict['biomarker'] = biomarker

                    new_dict['response'] = molecular_object['association']['evidence'][0]['description']
                    new_dict['evidence_statement'] = molecular_object['association']['description']

                    if "phenotypes" in molecular_object["association"]:
                        if len(molecular_object["association"]["phenotypes"]) > 0:
                            new_dict["disease"] = molecular_object["association"]["phenotypes"][0]["description"]

                    citation_ids = [molecular_object['association']['evidence'][i]['info']['publications']
                                               for i in range(len(molecular_object['association']['evidence']))][0]

                    for citation_id in citation_ids:
                        new_dict_id = copy.deepcopy(new_dict)
                        new_dict_id["citation_url"] = citation_id
                        new_dict_id["citation_id"] = extract_pmid(citation_id)
                        dict_list.append(new_dict_id)

                    #new_dict['variant'] = molecular_object['features'][0]['description']

                    #if (db_label=="CIViC") and (new_dict["gene"] == "BRAF") and (new_dict["variant_exchange"] == "V600E"):
                    #    print("entry for ",db_label,": ",new_dict["biomarker"],",", new_dict["gene"],",",new_dict["variant_exchange"],",",new_dict["drug"])
                    #dict_list.append(new_dict)
        return dict_list

    def load_biomarker_based_dictionary(self,dict_list):
        """


        :param dict_list:
        :return:
        """
        gene_dc = {}

        start = time.time()

        for i,entry in enumerate(dict_list):
            if "biomarker" in entry:
                if entry["biomarker"] not in gene_dc:
                    gene_dc[entry["biomarker"]] = []
                gene_dc[entry["biomarker"]].append(entry)

        print(time.time() - start)
        return gene_dc

    def load_any_mutation_in_gene_dictionary(self, dict_list):
        gene_dc = {}

        start = time.time()

        for i,entry in enumerate(dict_list):
            if ("gene" in entry) and ("variant_exchange" in entry):
                if entry["gene"] not in gene_dc:
                    gene_dc[entry["gene"]] = []
                gene_dc[entry["gene"] + " " + "variant_exchange"].append(entry)

        print(len(dict_list))
        #print(gene_dc.keys())
        #print(len(gene_dc["BRAF"]))

        print(time.time() - start)
        return gene_dc

    def load_gene_based_dictionary(self, db_label, dict_list):
        gene_dc = {}

        start = time.time()

        for i,entry in enumerate(dict_list):
            if "gene" in entry:
                if entry["gene"] not in gene_dc:
                    gene_dc[entry["gene"]] = []
                gene_dc[entry["gene"]].append(entry)

        print(len(dict_list))
        #print(gene_dc.keys())
        #print(len(gene_dc["BRAF"]))

        print(time.time() - start)
        return gene_dc

#db = LocalMetaKB()
#response = db.search_biomarker("BRAF V600E")
#response = search_gene("BRAF")
#print(response)


def metakb_request(metakb_dc, q, match_type):
    print(match_type + " request: biomarker: ", q)
    results = []

    for dc in metakb_dc.keys():
        if q in metakb_dc[dc]:
            new_results = metakb_dc[dc][q]

            for result in new_results:
                result["match_type"] = match_type

            results = results + new_results

    response = {"metakb_features_norm": {match_type: []}}
    response["metakb_features_norm"][match_type] = results

    return response


def exact_match_request_biomarker(biomarker_dc,biomarker):
    """

    :param biomarker_dc:
    :param biomarker:
    :return:
    """
    #print("exact match request: biomarker: ",biomarker)
    results = []

    for dc in biomarker_dc.keys():
        if biomarker in biomarker_dc[dc]:
            new_results = biomarker_dc[dc][biomarker]

            for result in new_results:
                result["match_type"] = "exact_match"

                if result["citation_id"] != "":
                    results = results + [result]

    response = {"metakb_features_norm": {"exact_match":[]}}
    response["metakb_features_norm"]["exact_match"] = results

    return response


def other_mutation_at_same_position():
    pass


def any_mutation_in_gene_request(gene_dc, gene):
    """
    Searches for clinical evidence data for treatments in a gene

    :param gene_dc
    :param gene:
    :return:
    """
    results = []

    for dc in gene_dc.keys():
        if gene in gene_dc[dc]:
            results = results + gene_dc[dc][gene]

    response = {"metakb_features_norm": {"any_mutation_in_gene":[]}}
    response["metakb_features_norm"]["any_mutation_in_gene"] = results
    return response
