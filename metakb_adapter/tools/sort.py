

def _sort_results_by_evidence_level(json_obj):
    """

    :param json_obj:
    :return:
    """
    return json_obj
    key = list(json_obj.keys())[0]
    sorted_response = {key: []}
    EVIDENCE_LABEL = ['A', 'B', 'C', 'D']
    for label in EVIDENCE_LABEL:
        label_filted_entrys = [
            entry for entry in json_obj[key] if entry['evidence_label'] == label]
        sorted_response[key].extend(label_filted_entrys)
    return sorted_response



