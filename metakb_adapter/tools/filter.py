import metakb_adapter.conf.read_config as conf_reader


def filter_empty_citation_ids(variant_data):

    for var in variant_data.keys():
        if "metakb_features_norm" in variant_data[var]:
            for match_type in conf_reader.match_types:
                if match_type in variant_data[var]["metakb_features_norm"]:
                    treatments_new = []
                    for t,result in enumerate(variant_data[var]["metakb_features_norm"][match_type]):
                        if "citation_id" in variant_data[var]["metakb_features_norm"][match_type][t]:
                            if "citation_id" != "":
                                treatments_new.append(result)
                    variant_data[var]["metakb_features_norm"][match_type] = treatments_new
    return variant_data
