import re, traceback
import metakb_adapter.conf.read_config as conf_reader


def merge_dictionaries(a, b, path=None):
    """
    Merges dictionary b into a without data loss

    :param a:
    :param b:
    :param path:
    :return:
    """
    if path is None: path = []
    for key in b:
        if key in a:
            if isinstance(a[key], dict) and isinstance(b[key], dict):
                merge_dictionaries(a[key], b[key], path + [str(key)])
            elif a[key] == b[key]:
                pass # same leaf value
            else:
                if isinstance(a[key], list) and isinstance(b[key], list):
                    a[key] = a[key] + b[key] # concatenate lists
                else:
                    raise Exception('Conflict at %s' % '.'.join(path + [str(key)]))
        else:
            a[key] = b[key]
    return a


def extract_pmid(url):
    """
    Extracts a PubMed-ID from a PubMed-URL

    :param url:
    :return:
    """
    regexp_pubmed_urls = conf_reader.pmid_expressions
    try:
        pmid = ''
        if url is not None:
            regex = regexp_pubmed_urls[0]
            m = re.compile(regex).match(str(url))
            if m is not None:
                pmid = m.group(4)
            # else:
            #    print("Could not match PubMedID (MetaKB): ",url)

            # Test asco URL
            regex = regexp_pubmed_urls[0]
            m = re.compile(regex).match(str(url))
            if m is not None:
                pmid = m.group(4)
            else:
                pass

            # Test PubMed-ID with no URL
            regex = regexp_pubmed_urls[2]
            m = re.compile(regex).match(str(url))
            if m is not None:
                pmid = m.group(1)
            else:
                pass

        return pmid
    except:
        print("Error extracting publication IDs: ", url)
        print(traceback.format_exc())
    return None
