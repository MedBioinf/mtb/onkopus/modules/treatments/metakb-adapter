import requests, copy
import metakb_adapter.conf.read_config as conf_reader
from metakb_adapter.tools.sort import _sort_results_by_evidence_level
from metakb_adapter.normalize import normalize


class MetaKB:
    """
    Class that starts for every input variant an API call to META-KB and stores
    the complete response. Includes also helper functions to return more relevant
    data.
    """

    def __init__(self, input_gene, input_variant=None, response_size=10, genompos=None, key_arg=None):
        """Parses the request and requests the META-KB API for a response object.

        Parameter arguments:
        input_gene -- the input gene as HUGO symbol (e.g. BRAF)
        input_variant -- the input variant of a gene (e.g. V600E)

        Keyword arguments:
        response_size -- the amount of responses returned by META-KB API
        """

        self.request_gene = input_gene
        self.request_variant = input_variant
        self.response_size = response_size
        self.genompos = genompos

        self.response = {}
        self.response_normalized = {}

        self.add_metakb_sections()

        if (key_arg is not None) and (key_arg == "genompos"):
            self.genompos_response_key = True
        else:
            self.genompos_response_key = False

        self.queryID = self.generate_qid(input_gene, input_variant)
        #self.response = self._exact_match_request()


    def __str__(self) -> str:
        """
        When called by print() return the response object of this class.
        """
        return_dict = {'gene': self.request_gene,
                       'variant': self.request_variant,
                       'size': self.response_size}
        return str(return_dict)

    def get_response_text(self):
        """Get the results of a response object and return a string."""
        return self.response.text

    def get_response_size(self):
        """Returns the response size of the META-KB response as int."""
        return self.response_size

    def get_request(self):
        """Returns the request of of the META-KB response as string."""
        return f"{self.request_gene}:{self.request_variant}"

    def get_sorted_response_json(self, sort_by='evidence'):
        """Sorts a json based on the given sort parameter and returns the sorted json"""

        for match_type in conf_reader.match_types:
            unsorted_response = self.get_response_json(match_type)

            if unsorted_response is not None:
                if sort_by == 'evidence':
                    sorted_response = _sort_results_by_evidence_level(unsorted_response)

                elif sort_by == 'cancer':
                    pass
                else:
                    raise NameError(f'''{sort_by} is not a valid parameter. 
                        Use "evidence", "response" or "cancer"''')

                self.response[match_type] = copy.deepcopy(sorted_response)
                #return sorted_response

    def generate_qid(self, input_gene, input_variant):
        if (self.genompos is not None) and self.genompos_response_key:
            self.qid = self.genompos
        else:
            self.qid = input_gene  + ":" + input_variant

    def get_api_response(self):
        dc = { self.qid: {} }
        dc[self.qid]["metakb"] = self.response
        return dc

    def get_full_response(self):
        dc = { self.qid: {} }
        dc[self.qid]["metakb"] = self.response
        dc[self.qid]["metakb_features_norm"] = self.response_normalized
        return dc

    def get_response_json(self, response):
        """
        Get the results of a response object, change the key to input variant
        and return again a json-object.
        """
        return response.json()['hits']['hits']

    def _parse_request(self, request):
        """"
        Check correct input variant and split it, return gene and variant as string.
        """
        gene, variant = request.split(":")
        return gene, variant

    def exact_match_request(self):
        """
        Sends a requests to MetaKB-API and returns the JSON response
        """
        request_uri = "https://search.cancervariants.org/api/v1/associations?size=" + str(self.response_size) + "&q=genes:" + self.request_gene

        # Check if request_variant is set and add the necessary API request
        if self.request_variant:
            request_uri += " AND association.variant_name:" + self.request_variant

        print(request_uri)
        response = requests.get(request_uri)
        if response.status_code == 200:
            unsorted_response = self.get_response_json(response)
            self.response["exact_match"] = unsorted_response
            return response
        else:
            return None

    def gene_request(self):
        request_url = "https://search.cancervariants.org/api/v1/associations?size="+ str(self.response_size) + "&q=genes:" + self.request_gene

        response = requests.get(request_url)
        print("elapsed time for MetaKB request ",response.elapsed)
        self.response["nearby_mutations"] = response.json()['hits']['hits']

        if response.status_code == 200:
            return response.json()['hits']['hits']
        else:
            return {}

    def add_metakb_sections(self):
        """
        Generates the MetaKB response sections, including different match types and normalized result sections

        :param variant_data:
        :return:
        """
        res = {}

        for match_type in conf_reader.match_types:
            res[match_type] = []
        self.response = res

    def normalize(self):
        response = normalize( copy.deepcopy(self.get_api_response()) )
        self.response_normalized = response[self.qid]["metakb_features_norm"]

    def filter_missing_values(self, variant_data):

        for var in variant_data.keys():
            if "metakb_features_norm" in variant_data[var]:
                results_new = []
                for i,_ in enumerate(variant_data[var]["metakb_features_norm"]):
                    # Check values for citation ID and drugs are present
                    if variant_data[var]["metakb_features_norm"][i]["citation_id"] != "":
                        if variant_data[var]["metakb_features_norm"][i]["drugs"] != "":
                            results_new.append(variant_data[var]["metakb_features_norm"][i])
                variant_data[var]["metakb_features_norm"] = results_new

        return variant_data

def main():
    """Generate basic example and run it with the class."""
    variant_gene = "BRAF"
    variant_mutation = None
    size = 10
    metakb_obj = MetaKB(variant_gene, variant_mutation, size)
    print(metakb_obj.get_sorted_response_json())


if __name__ == '__main__':
    main()
