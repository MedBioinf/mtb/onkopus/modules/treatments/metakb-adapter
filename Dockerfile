FROM python:3.9-buster

WORKDIR /app
COPY . /app/

RUN     pip install -r res/requirements/requirements

EXPOSE 8100
ENTRYPOINT ["python", "/app/app.py"]
