# MetaKB-DB Service

Adapter for retrieving clinical significance data from the MetaKB API presented in [1]. 

References
[1] Wagner, A. H., Walsh, B., Mayfield, G., Tamborero, D., Sonkin, D., Krysiak, K., ... & Variant Interpretation for Cancer Consortium. (2020). A harmonized meta-knowledgebase of clinical interpretations of somatic genomic variants in cancer. Nature genetics, 52(4), 448-457.


## Getting started

docker build -t metakb .
docker run --name metakb --rm -p 8100:8100 metakb

### Data Processing

The adapter extracts a set of features from the MetaKB API response and stores it in the normalized feature section ("metakb_feature_norm"). 
The MetaKB adapter performs the following steps for feature normalization: 
- Mapping of feature keys: Map features from MetaKB to the Onkopus features for evidence-based data
- Mapping of feature values: Format values in the same way for all evidence-based modules
- Filter missing values: The adapter filters all results that do not include a drug or a citation ID
- Mapping of evidence levels to Onkopus evidence level score system

#### Mapping of feature keys

The following MetaKB features are mapped to the normalized feature section:

geneSymbol | Gene

variant_name | Variant

diseases | Disease

drugs | Drugs

response_type | Response Type

evidence_level | Evidence Level

publications | Citation ID

#### Mapping of feature values



